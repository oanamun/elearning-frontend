var adminApp = angular.module("adminApp", ['ngRoute', 'ngStorage']);

adminApp.config(function ($routeProvider) {
    $routeProvider
        .when('/users', {
            title: 'Management useri',
            templateUrl: 'templates/users.html',
            controller: 'UsersController'
        })
        .when('/add-user', {
            title: 'Adauga user',
            templateUrl: 'templates/add-user.html',
            controller: 'UsersController'
        })
        .when('/permissions', {
            title: 'Permisiuni',
            templateUrl: 'templates/permissions.html',
            controller: 'PermissionsController'
        })
        .when('/logs', {
            title: 'Log-uri',
            templateUrl: 'templates/logs.html',
            controller: 'LogsController'
        })
        .otherwise({
            redirectTo: '/users'
        });
});

adminApp.run(['$rootScope', '$route', function ($rootScope, $route) {
    $rootScope.$on('$routeChangeSuccess', function () {
        document.title = $route.current.title;
    });
}]);


adminApp.factory('UserService', function ($localStorage) {
    return {
        getUser: function () {
            return $localStorage.user;
        },
        getToken: function () {
            return $localStorage.token;
        }
    }
});

adminApp.controller('UsersController', function ($scope, $http, UserService) {
    $scope.users = [];
    $scope.user={};
    $scope.sortOrder=1;
    $scope.selectOption = '';
    $scope.inputText = '';
    $scope.originalUsers = {};

    $http.get("http://localhost:60163/users/details", {
        headers: {
            "Token": UserService.getToken()
        }
    }).success(function (data) {
        for(var i=0;i<data.length;i++){
            if(data[i].Username=="admin"){
                data.splice(i, 1);
                break;
            }
        }
        $scope.users = data;
        $scope.originalUsers=data;
    });

    $scope.addUser = function () {
        $scope.user.Suspended="false";
        $scope.user.Blocked="false";

        if($scope.user.Gender=="F"){
            $scope.user.Gender=true;
        }else{
            $scope.user.Gender=false;
        }

        $http.post("http://localhost:60163/users", $scope.user, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.users.push(data);
                alert("Utilizatorul a fost adaugat!")
            })
            .error(function () {
                alert("Eroare add user!")
            });
    };

    $scope.changeSuspend = function(index){
        var userId = $scope.users[index].Id;
        $http.get("http://localhost:60163/users/"+userId+"/suspend", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
                if($scope.users[index].Suspended==true){
                    alert("Utilizatorul a fost suspendat pentru o luna!")
                }else{
                    alert("Utilizatorul nu mai este suspendat!")
                }
            });
    };

    $scope.changeBlock = function(index){
        var userId = $scope.users[index].Id;
        $http.get("http://localhost:60163/users/"+userId+"/block", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
                if($scope.users[index].Blocked==true){
                    alert("Utilizatorul a fost blocat!")
                }else{
                    alert("Utilizatorul a fost deblocat!")
                }
            });
    };

    $scope.resetPassword = function(index){
        var userId = $scope.users[index].Id;
    };

    $scope.sortColumn = function (columnName) {
        var order = $scope.sortOrder;
        var users = $scope.users.sort(function (item1, item2) {
            var a = item1[columnName] || '';
            var b = item2[columnName] || '';
            return (a > b ? 1 : a == b ? 0 : -1) * order;
        });
        this.sortOrder = order * -1;
    };

    $scope.search = function(){
        var inputText = $scope.inputText;
        var new_users = $scope.originalUsers.reduce(function(acc, user){
            const u = JSON.stringify(user);
            if (u.indexOf(inputText) >= 0) {
                acc.push(user);
            }
            return acc;
        }, [])
        $scope.users = new_users;
    };

    $scope.restore = function(){
        $scope.users = $scope.originalUsers;
    };

    $scope.setProperty = function(propName, event){
        var value = event.target.value;
        $scope.inputText = value;

    }

});


adminApp.controller('PermissionsController', function ($scope, $http, UserService, $location) {
    $scope.courses = [];
    $scope.permission = {};
    $scope.users=[];
    $scope.permissions=[];
    $scope.roles =['Profesor','Asistent','Student'];

    var getPermissions=function(){
        $http.get("http://localhost:60163/courses/permissions", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.permissions = data;
            })
            .error(function () {
                console.log("Eroare get courses!")
            });
    };
    getPermissions();

    $http.get("http://localhost:60163/courses", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.courses = data;
        })
        .error(function () {
            console.log("Eroare get courses!")
        });

    $http.get("http://localhost:60163/users", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.users = data;
        });

    $scope.addPermission = function () {
        $http.post("http://localhost:60163/api/permissions", $scope.permission, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
                alert("Permisiunea a fost adaugata!");
                getPermissions();
            })
            .error(function () {
                console.log("Eroare add permission!")
            });
    }
});

adminApp.controller('LogsController', function ($scope, $http, UserService) {
    $scope.logs = [];
    $scope.log = {};

    $http.get("http://localhost:60163/api/logs", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.logs = data;
        });
});

adminApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown", function (e) {
            if (e.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter, {'e': e});
                });
                e.preventDefault();
            }
        });
    };
});
var loginApp = angular.module("loginApp", ['ngRoute', 'ngStorage']);

loginApp.config(function ($routeProvider) {
    $routeProvider
        .when('/login', {
            templateUrl: 'templates/login.html',
            controller: 'LoginController'
        })
        .when('/reset-password', {
            templateUrl: 'templates/forgot_password.html',
            controller: 'LoginController'
        })
        .otherwise({
            redirectTo: '/login'
        });
});

loginApp.factory('LoginService', function ($localStorage) {
    return {
        setUser: function (value) {
            $localStorage.user = value;
        },
        setToken: function (value) {
            $localStorage.token = value;
        }
    }
});


loginApp.controller('LoginController', function (LoginService, $scope, $http, $window) {
    $scope.username = "";
    $scope.password = "";
    $scope.loading = false;

    $scope.login = function () {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $scope.username + ':' + $scope.password;
        $scope.loading = true;
        $http.post("http://localhost:60163/api/login")
            .success(function (data, status, headers) {
                LoginService.setToken(headers('Token'));
                LoginService.setUser(data);
                if (data.Suspended) {
                    alert("Acest cont este suspendat!");
                }
                else if (data.Blocked) {
                    alert("Acest cont este blocat!");
                }
                else {
                    $scope.changePath(headers('Token'));
                }

            })
            .error(function () {
                $scope.loading = false;
                alert("Login-ul nu s-a facut cu succes!")
            });
    };

    $scope.changePath = function (token) {
        $http.get("http://localhost:60163/users/user/permissions", {
            headers: {
                "Token": token
            }
        }).success(function (data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].Role == "Admin") {
                    $window.location.href = '/elearning-frontend/admin.html';
                    return;
                }
            }
            $window.location.href = '/elearning-frontend/index.html';
        });

    };

});

loginApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown", function (e) {
            if (e.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter, {'e': e});
                });
                e.preventDefault();
            }
        });
    };
});


var userApp = angular.module("userApp", ['ngRoute', 'ngStorage', 'ngSanitize', 'luegg.directives']);

userApp.config(function ($routeProvider) {
    //$locationProvider.html5Mode(true);
    $routeProvider
        .when('/courses', {
            title: 'Cursuri',
            templateUrl: 'templates/courses.html',
            controller: 'CoursesController',
            resolve: {
                mess: function ($localStorage, $window) {
                    var t = $localStorage.token;
                    if (t === '') {
                        $window.location.href = '/elearning-frontend/login.html';
                    }
                }

            }
        })
        .when('/all-courses', {
            title: 'Cursuri',
            templateUrl: 'templates/register-course.html',
            controller: 'CoursesController'
        })
        .when('/course/:course_id', {
            title: 'Curs',
            templateUrl: 'templates/course.html',
            controller: 'CourseController'
        })
        .when('/course/:course_id/discussions', {
            title: 'Curs',
            templateUrl: 'templates/course-discussions.html',
            controller: 'CourseController'
        })
        .when('/course/:course_id/announcements', {
            title: 'Curs',
            templateUrl: 'templates/course-announcements.html',
            controller: 'CourseController'
        })
        .when('/course/:course_id/students', {
            title: 'Studenti',
            templateUrl: 'templates/course-students.html',
            controller: 'CourseController'
        })
        .when('/add-course', {
            title: 'Adauga curs',
            templateUrl: 'templates/add-course.html',
            controller: 'CoursesController'
        })
        .when('/module/:module_id', {
            title: 'Modul',
            templateUrl: 'templates/module.html',
            controller: 'ModuleController'
        })
        .when('/module/:module_id/teste', {
            title: 'Test',
            templateUrl: 'templates/module-test.html',
            controller: 'ModuleController'
        })
        .when('/module/:module_id/teme', {
            title: 'Tema',
            templateUrl: 'templates/module-homework.html',
            controller: 'ModuleController'
        })
        .when('/add-module/:course_id', {
            title: 'Adauga modul',
            templateUrl: 'templates/add-module.html',
            controller: 'CourseController'
        })
        .when('/edit-module/:module_id', {
            title: 'Editeaza modul',
            templateUrl: 'templates/edit-module.html',
            controller: 'ModuleController'
        })
        .when('/add-test/:module_id', {
            title: 'Adauga test',
            templateUrl: 'templates/add-test.html',
            controller: 'TestController'
        })
        .when('/edit-test/:test_id', {
            title: 'Editeaza test',
            templateUrl: 'templates/edit-test.html',
            controller: 'TestController'
        })
        .when('/add-homework/:module_id', {
            title: 'Adauga tema',
            templateUrl: 'templates/add-homework.html',
            controller: 'HomeworkController'
        })
        .when('/edit-homework/:homework_id', {
            title: 'Editeaza tema',
            templateUrl: 'templates/edit-homework.html',
            controller: 'HomeworkController'
        })
        .when('/homework/:homework_id', {
            title: 'Tema',
            templateUrl: 'templates/homework-page.html',
            controller: 'HomeworkController'
        })
        .when('/grade-homework/:homework_id', {
            title: 'Teme',
            templateUrl: 'templates/grade-homework.html',
            controller: 'HomeworkController'
        })
        .when('/forum', {
            title: 'Forum',
            templateUrl: 'templates/forum.html',
            controller: 'DiscussionsController'
        })
        .when('/discussion/:discussion_id', {
            title: 'Discutie',
            templateUrl: 'templates/discussion.html',
            controller: 'DiscussionController'
        })
        .when('/add-discussion', {
            title: 'Adauga discutie',
            templateUrl: 'templates/add-discussion.html',
            controller: 'DiscussionsController'
        })
        .when('/calendar', {
            title: 'Calendar',
            templateUrl: 'templates/calendar.html',
            controller: 'CalendarController'
        })
        .when('/messages', {
            title: 'Mesaje',
            templateUrl: 'templates/messages.html',
            controller: 'MessagesController'
        })
        .when('/messages/:conversation_id', {
            title: 'Mesaje',
            templateUrl: 'templates/messages.html',
            controller: 'MessagesController'
        })
        .when('/add-conversation', {
            title: 'Mesaje',
            templateUrl: 'templates/add-conversation.html',
            controller: 'ConversationController'
        })
        .when('/tests', {
            title: 'Teste',
            templateUrl: 'templates/tests.html',
            controller: 'TestsController'
        })
        .when('/homework', {
            title: 'Teme',
            templateUrl: 'templates/homework.html',
            controller: 'HomeworkController'
        })
        .when('/grades', {
            title: 'Note',
            templateUrl: 'templates/grades.html',
            controller: 'GradesController'
        })
        .when('/grades/:course_id', {
            title: 'Note curs',
            templateUrl: 'templates/course-grades.html',
            controller: 'StudentGradesController'
        })
        .when('/grades-final/:course_id', {
            title: 'Note',
            templateUrl: 'templates/grades-final.html',
            controller: 'TeacherGradesController'
        })
        .when('/grades-course/:course_id', {
            title: 'Note',
            templateUrl: 'templates/grades-course.html',
            controller: 'TeacherGradesController'
        })
        .when('/grades/:course_id/user/:user_id', {
            title: 'Note',
            templateUrl: 'templates/grades-user.html',
            controller: 'TeacherGradesController'
        })
        .when('/grades-course/:course_id/:elem_type/:elem_id', {
            title: 'Note',
            templateUrl: 'templates/grades-element.html',
            controller: 'TeacherGradesController'
        })
        .when('/group', {
            title: 'Grupa',
            templateUrl: 'templates/group.html',
            controller: 'GroupController'
        })
        .when('/group/pages', {
            title: 'Grupa',
            templateUrl: 'templates/group-pages.html',
            controller: 'GroupController'
        })
        .when('/page/:page_id', {
            title: 'Pagina',
            templateUrl: 'templates/page.html',
            controller: 'PagesController'
        })
        .when('/edit-page/:page_id', {
            title: 'Pagina',
            templateUrl: 'templates/edit-page.html',
            controller: 'PagesController'
        })
        .when('/group/announcements', {
            title: 'Grupa',
            templateUrl: 'templates/group-announcements.html',
            controller: 'GroupController'
        })
        .when('/add-page', {
            title: 'Adauga pagina',
            templateUrl: 'templates/add-page.html',
            controller: 'PagesController'
        })
        .when('/tests', {
            title: 'Teste',
            templateUrl: 'templates/tests.html',
            controller: 'TestsController'
        })
        .when('/test/:test_id', {
            title: 'Test',
            templateUrl: 'templates/test.html',
            controller: 'TestController'
        })
        .when('/test/:test_id/question/:question_nr', {
            title: 'Test',
            templateUrl: 'templates/question.html',
            controller: 'QuestionsController'
        })
        .when('/help', {
            title: 'Help',
            templateUrl: 'templates/help.html',
            controller: ''
        })
        .when('/profile', {
            title: 'Profil',
            templateUrl: 'templates/profile.html',
            controller: 'ProfileController'
        })
        .when('/profile/:user_id', {
            title: 'Profil',
            templateUrl: 'templates/user-profile.html',
            controller: 'ProfileController'
        })
        .when('/notifications', {
            title: 'Notificari',
            templateUrl: 'templates/notifications.html',
            controller: 'NotificationsController'
        })
        .when('/videoconf', {
            title: 'Videoconferinta',
            templateUrl: 'templates/videoconf.html'
        })
        .otherwise({
            redirectTo: '/courses'
        });
});

userApp.run(['$rootScope', '$route', function ($rootScope, $route) {
    $rootScope.$on('$routeChangeSuccess', function () {
        document.title = $route.current.title;
    });
}]);


userApp.factory('UserService', function ($localStorage) {
    return {
        setUser: function (value) {
            $localStorage.user = value;
        },
        setToken: function (value) {
            $localStorage.token = value;
        },
        getUser: function () {
            return $localStorage.user;
        },
        getToken: function () {
            return $localStorage.token;
        }
    }
});


userApp.controller('MainController', function (UserService, $scope, $window) {
    $scope.user = UserService.getUser();

    $scope.logout = function () {
        UserService.setToken('');
        UserService.setUser('');
    }

});


userApp.directive('loadCalendar', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element).fullCalendar(scope.$eval({
                theme: true,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                editable: true,
                eventLimit: true,
                timeFormat: 'H:mm'
            }));
        }
    };
});


userApp.directive('textEditor', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element).trumbowyg(scope.$eval({
                fullscreenable: true,
                btns: ['viewHTML',
                    '|', 'formatting',
                    '|', 'btnGrp-design',
                    '|', 'link',
                    '|', 'insertImage',
                    '|', 'btnGrp-justify',
                    '|', 'btnGrp-lists',
                    '|', 'horizontalRule']
            }));
        }
    };
});


userApp.directive('slimScroll', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element).slimScroll({
                height: '300px',
                color: '#00D6B2'
            });
        }
    };
});


userApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown", function (e) {
            if (e.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter, {'e': e});
                });
                e.preventDefault();
            }
        });
    };
});


userApp.controller('CoursesController', function ($scope, $http, $location, UserService) {
    $scope.courses = [];
    $scope.course = {};
    $scope.new_courses = [];

    $http.get("http://localhost:60163/courses/mycourses", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.courses = data;
        })
        .error(function () {
            console.log("Eroare get courses!")
        });

    $http.get("http://localhost:60163/courses/others", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.new_courses = data;
        })
        .error(function () {
            console.log("Eroare get courses!")
        });


    $scope.register = function (courses) {
        for (var i in courses) {
            if (courses[i].SELECTED == 'Y') {
                console.log(UserService.getUser().username);
                var permission = {Role: "Student", course_id: courses[i].Id, user_id: UserService.getUser().Id};
                $http.post("http://localhost:60163/api/permissions", permission, {
                        headers: {
                            "Token": UserService.getToken()
                        }
                    })
                    .success(function () {
                        $location.path('/courses');
                    })
                    .error(function () {
                        console.log("Eroare add permission!")
                    });
            }
        }
    }

    $scope.addCourse = function () {
        $http.post("http://localhost:60163/courses", $scope.course, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                var permission = {Role: "Profesor", course_id: data.Id, user_id: UserService.getUser().Id};
                $http.post("http://localhost:60163/api/permissions", permission, {
                        headers: {
                            "Token": UserService.getToken()
                        }
                    })
                    .success(function () {
                    })
                    .error(function () {
                        console.log("Eroare add permission!")
                    });
                $scope.courses.push(data);
            })
            .error(function () {
                console.log("Eroare add course!")
            });
        $location.path('/courses');
    };

    $scope.selectedCourse = function (index) {
        var courseId = $scope.courses[index].Id;
        $location.path('/course/' + courseId);
    };

});

//-------------------------------------------------------------------------------------

userApp.controller('CourseController', function ($scope, $http, $location, $routeParams, UserService) {
    $scope.course = {};
    $scope.module = {};
    $scope.announcement = {};
    $scope.modules = [];
    $scope.discussions = [];
    $scope.announcements = [];
    $scope.courseId = $routeParams.course_id;
    $scope.professor = false;
    $scope.students = [];

    $http.get("http://localhost:60163/courses/" + $scope.courseId, {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.course = data;
        })
        .error(function () {
            console.log("Eroare get course modules!")
        });

    if (!UserService.getUser().Student) {
        $http.get("http://localhost:60163/courses/" + $scope.courseId + "/users", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.students = data;
            });
    }

    $http.get("http://localhost:60163/courses/mycourses/" + $scope.courseId + "/permission", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            if (data.Role === "Profesor") {
                $scope.professor = true;
            }

        })
        .error(function () {
            console.log("Eroare get permissions!")
        });

    var getModules = function () {
        $http.get("http://localhost:60163/courses/" + $scope.courseId + "/modules", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.modules = data;
            })
            .error(function () {
                console.log("Eroare get course modules!")
            });
    }
    getModules();

    $http.get("http://localhost:60163/courses/" + $scope.courseId + "/discussions", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.discussions = data;
        })
        .error(function () {
            console.log("Eroare get course discussions!")
        });

    $http.get("http://localhost:60163/courses/" + $scope.courseId + "/announcements", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.announcements = data;
        })
        .error(function () {
            console.log("Eroare get course announcements!")
        });

    $scope.addModule = function () {
        $scope.module.course_id = $scope.courseId;
        $scope.module.Content = $('.text-editor').trumbowyg('html');
        console.log($scope.module.content);
        $http.post("http://localhost:60163/modules", $scope.module, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                getModules();
                $location.path('/course/' + $scope.courseId);
            })
            .error(function () {
                console.log("Eroare add module!")
            });

        $location.path('/course/' + $scope.courseId);
    };

    $scope.addDiscussion = function () {
        $scope.discussion.course_id = $scope.courseId;

        $http.post("http://localhost:60163/forum", $scope.discussion, {
            headers: {
                "Token": UserService.getToken()
            }
        }).success(function (data) {
                $scope.discussions.push(data);
                $scope.discussion = {};
            })
            .error(function () {
                console.log("Eroare add discussion!")
            });
    };

    $scope.addAnnouncement = function () {
        $scope.announcement.course_id = $scope.courseId;
        $http.post("http://localhost:60163/announcements/" + $scope.courseId + "/announcement", $scope.announcement, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.announcements.push(data);
                $scope.announcement = {};
            })
            .error(function () {
                console.log("Eroare add annoucement!")
            });
    }

    $scope.addCourseDiscussion = function () {
        $location.path('/add-discussion/' + $scope.courseId);
    };

    $scope.addCourseAnnouncement = function () {
        $location.path('/add-announcement/' + $scope.courseId);
    };

    $scope.addModuleButton = function () {
        $location.path('/add-module/' + $scope.courseId);
    };

    //go to module page
    $scope.selectedModule = function (index) {
        var id = $scope.modules[index].Id;
        $location.path('/module/' + id);
    };

    //go to discussion page
    $scope.selectedDiscussion = function (index) {
        var id = $scope.discussions[index].Id;
        $location.path('/discussion/' + id);
    };

    //go to course modules
    $scope.courseModule = function () {
        $location.path('/course/' + $scope.courseId);
    };

    //go to course discussons
    $scope.courseDiscussions = function () {
        $location.path('/course/' + $scope.courseId + '/discussions');
    };

    //go to course announcements
    $scope.courseAnnouncements = function () {
        $location.path('/course/' + $scope.courseId + '/announcements');
    };

    $scope.courseStudents = function () {
        $location.path('/course/' + $scope.courseId + '/students');
    };

    $scope.seeUserProfile = function (index) {
        var id = $scope.students[index].Id;
        $location.path('/profile/' + id);
    };

});

//-------------------------------------------------------------------------------------

userApp.controller('ModuleController', function ($scope, $http, $location, $routeParams, UserService, $sce) {
    $scope.module = {};
    $scope.tests = [];
    $scope.homeworks = [];
    $scope.moduleId = $routeParams.module_id || "";
    $scope.professor = false;

    $http.get("http://localhost:60163/modules/" + $scope.moduleId, {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.module = data;
            $scope.module.Content = $sce.trustAsHtml(data.Content);
            $('.text-editor').trumbowyg('html', $scope.module.Content);
            $scope.getPermission();
        })
        .error(function () {
            console.log("Eroare get modul!")
        });


    $scope.editModule = function () {
        $scope.module.Content = $('.text-editor').trumbowyg('html');

        $http.put("http://localhost:60163/modules/" + $scope.moduleId, $scope.module, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
                $location.path('/module/' + $scope.moduleId);
            })
            .error(function () {
                console.log("Eroare put page!")
            });
    }

    $scope.openEdit = function () {
        $location.path('/edit-module/' + $scope.moduleId);
    }


    $scope.moduleContent = function () {
        console.log($scope.module.content);
        return $sce.trustAsHtml($scope.module.content);
    };

    $scope.getPermission = function () {
        $http.get("http://localhost:60163/courses/mycourses/" + $scope.module.course_id + "/permission", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                if (data.Role === "Profesor") {
                    $scope.professor = true;
                }

            })
            .error(function () {
                console.log("Eroare get permissions!")
            });
    };

    //get test
    $http.get("http://localhost:60163/modules/" + $scope.moduleId + "/tests", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.tests = data;
        })
        .error(function () {
            console.log("Eroare get test!")
        });

    //get hw
    $http.get("http://localhost:60163/modules/" + $scope.moduleId + "/homeworks", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.homeworks = data;
        })
        .error(function () {
            console.log("Eroare get hw!")
        });

    $scope.moduleTests = function () {
        $location.path('/module/' + $scope.moduleId + '/teste');
    };

    $scope.moduleHomework = function () {
        $location.path('/module/' + $scope.moduleId + '/teme');
    };

    $scope.moduleCourse = function () {
        $location.path('/module/' + $scope.moduleId);
    };

    $scope.startTest = function (index) {
        var testId = $scope.tests[index].Id;
        $location.path('/test/' + testId);
    }

    $scope.solveHomework = function (index) {
        var hwId = $scope.homeworks[index].Id;
        $location.path('/homework/' + hwId);
    }


    $scope.addTest = function () {
        $location.path('/add-test/' + $scope.moduleId);
    };

    $scope.addHomework = function () {
        $location.path('/add-homework/' + $scope.moduleId);
    };

});

userApp.controller('DiscussionsController', function ($scope, $http, UserService, $location, $routeParams) {
    $scope.group_discussions = [];
    $scope.discussion = {}
    $scope.comment = {};

    $http.get("http://localhost:60163/forum", {
        headers: {
            "Token": UserService.getToken()
        }
    }).success(function (data) {
            $scope.group_discussions = data;
        })
        .error(function () {
            console.log("Eroare get discutii grup!")
        });


    $scope.addDiscussion = function () {
        if ($routeParams.discussion_id) {
            $scope.discussion.course_id = $routeParams.discussion_id;
        }

        $http.post("http://localhost:60163/forum", $scope.discussion, {
            headers: {
                "Token": UserService.getToken()
            }
        }).success(function (data) {
                $scope.group_discussions.push(data);
                $scope.discussion = {};
            })
            .error(function () {
                console.log("Eroare add discussion!")
            });
    };

    // go to discussion page
    $scope.selectedDiscussion = function (index) {
        var discussionId = $scope.group_discussions[index].Id;
        $location.path('/discussion/' + discussionId);
    };

});

//-------------------------------------------------------------------------------------------

userApp.controller('DiscussionController', function ($scope, $http,$location, UserService, $routeParams) {
    $scope.comment = {};
    $scope.discussion = {};
    $scope.comments = [];
    $scope.discussionId = $routeParams.discussion_id;
    $scope.professor = false;
    $scope.users = [];
    $scope.user ="";
    $scope.grade = {};
    $scope.currentPage = 0;
    $scope.pageSize = 5;

    $http.get("http://localhost:60163/forum/" + $scope.discussionId, {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.discussion = data;
            $scope.getPermission();
        });

    $http.get("http://localhost:60163/forum/" + $scope.discussionId + "/comments", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.comments = data;
        })
        .error(function () {
            console.log("Eroare get comments!")
        });

    if(!UserService.getUser().Student) {
        $http.get("http://localhost:60163/forum/" + $scope.discussionId + "/participants", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.users = data;
            })
            .error(function () {
                console.log("Eroare get comments!")
            });
    }

    $scope.addComment = function () {
        $scope.comment.discussion_id = $scope.discussionId;
        $scope.comment.Date = new Date();
        $scope.comment.user_id = UserService.getUser().Id;
        $http.post("http://localhost:60163/api/comments", $scope.comment, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                data.user_name = UserService.getUser().Username;
                data.group_name = UserService.getUser().group_name;
                $scope.comments.push(data);
            }).error(function () {
            console.log("Eroare add comment!")
        });
        $scope.comment = "";
    };

    $scope.addGrade = function () {
        $scope.grade.user_id = $scope.user;
        $scope.grade.discussion_id = $scope.discussionId;

        $http.post("http://localhost:60163/grades", $scope.grade, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
            });
    };

    $scope.deleteDiscussion = function () {
        var courseId = $scope.discussion.course_id;

        $http.delete("http://localhost:60163/forum/" + $scope.discussionId, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
                $location.path('/course/' + courseId +'/discussions');
            });

    };

    $scope.getPermission = function () {
        $http.get("http://localhost:60163/courses/mycourses/" + $scope.discussion.course_id + "/permission", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                if (data.Role === "Profesor" || data.Role === "Asistent") {
                    $scope.professor = true;
                }

            })
            .error(function () {
                console.log("Eroare get permissions!")
            });
    };

    $scope.numberOfPages=function(){
        return Math.ceil($scope.comments.length/$scope.pageSize);
    }
});

userApp.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});
userApp.controller('TeacherGradesController', function (UserService, $scope, $http, $location, $routeParams) {
        $scope.grades = [];
        $scope.courseId = $routeParams.course_id || "";
        $scope.professor = !UserService.getUser().Student;
        $scope.elements = [];
        $scope.userId = $routeParams.user_id || "";
        $scope.elemId = $routeParams.elem_id || "";
        $scope.elemType = $routeParams.elem_type || "";
        $scope.courseName = "";

        var getGrades = function () {
            $http.get("http://localhost:60163/grades/" + $scope.courseId + "/students/finals", {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.grades = data;
                })
                .error(function () {
                    console.log("Eroare get courses!")
                });

        };

        if ($scope.courseId) {
            $http.get("http://localhost:60163/courses/" + $scope.courseId, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.courseName = data.Name;
                });

            if ($scope.userId) {
                $http.get("http://localhost:60163/grades/" + $scope.courseId + "/" + $scope.userId + "/details", {
                        headers: {
                            "Token": UserService.getToken()
                        }
                    })
                    .success(function (data) {
                        $scope.grades = data;
                    });
            }
            else if ($scope.elemId) {
                if ($scope.elemType == "Tema") {
                    $http.get("http://localhost:60163/grades/homeworks/" + $scope.elemId, {
                            headers: {
                                "Token": UserService.getToken()
                            }
                        })
                        .success(function (data) {
                            $scope.grades = data;
                        })

                }
                else if ($scope.elemType == "Test") {
                    $http.get("http://localhost:60163/grades/tests/" + $scope.elemId, {
                            headers: {
                                "Token": UserService.getToken()
                            }
                        })
                        .success(function (data) {
                            $scope.grades = data;
                        })
                }
                else if ($scope.elemType == "Discutie") {
                    $http.get("http://localhost:60163/grades/discussions/" + $scope.elemId, {
                            headers: {
                                "Token": UserService.getToken()
                            }
                        })
                        .success(function (data) {
                            $scope.grades = data;
                        })
                }

            }
            else {
               getGrades();

                $http.get("http://localhost:60163/courses/" + $scope.courseId + "/elements", {
                        headers: {
                            "Token": UserService.getToken()
                        }
                    })
                    .success(function (data) {
                        $scope.elements = data;
                    })
                    .error(function () {
                        console.log("Eroare get courses!")
                    });

            }
        }

        $scope.getFinalGrades = function () {
            $http.get("http://localhost:60163/courses/" + $scope.courseId + "/calculateFinals", {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    getGrades();
                });
        };

        $scope.userGradesPage = function (index) {
            var userId = $scope.grades[index].user_id;
            $location.path('/grades/' + $scope.courseId + "/user/" + userId);
        };
        $scope.courseFinalPage = function () {
            $location.path('/grades-final/' + $scope.courseId);
        };
        $scope.courseGradesPage = function () {
            $location.path('/grades-course/' + $scope.courseId);
        };

        $scope.courseElementPage = function (index) {
            var elemId = $scope.elements[index].Id;
            var elemType = $scope.elements[index].Type;
            $location.path('/grades-course/' + $scope.courseId + "/" + elemType + "/" + elemId);
        };
    }
);

userApp.controller('StudentGradesController', function (UserService, $scope, $http, $location, $routeParams) {
        $scope.grades = [];
        $scope.courseId = $routeParams.course_id || "";


        $http.get("http://localhost:60163/grades/finals", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.grades = data;
            })
            .error(function () {
                console.log("Eroare get modul!")
            });

        if ($scope.courseId) {
            $http.get("http://localhost:60163/courses/" + $scope.courseId + "/grades", {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.grades = data;
                })
                .error(function () {
                    console.log("Eroare get grade!")
                });
        }
    }
);


userApp.controller('GradesController', function (UserService, $scope, $http, $location, $routeParams) {
        $scope.grades = [];
        $scope.professor = !UserService.getUser().Student;

        if (UserService.getUser().Student) {
            $http.get("http://localhost:60163/grades/finals", {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.grades = data;
                })
                .error(function () {
                    console.log("Eroare get modul!")
                });

            if ($scope.courseId) {
                $http.get("http://localhost:60163/courses/" + $scope.courseId + "/grades", {
                        headers: {
                            "Token": UserService.getToken()
                        }
                    })
                    .success(function (data) {
                        $scope.grades = data;
                    })
                    .error(function () {
                        console.log("Eroare get grade!")
                    });
            }
            $scope.courseGradesPage = function (index) {
                var courseId = $scope.grades[index].course_id;
                $location.path('/grades/' + courseId);
            };
        } else {
            $http.get("http://localhost:60163/courses/mycourses", {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.courses = data;
                })
                .error(function () {
                    console.log("Eroare get courses!")
                });
            $scope.profGradesPage = function (index) {
                var courseId = $scope.courses[index].Id;
                $location.path('/grades-final/' + courseId);
            };
        }
    }
);
userApp.controller('GroupController', function (UserService, $scope, $http, $location) {
    $scope.groupId = UserService.getUser().group_id;
    $scope.group = {};
    $scope.users = [];
    $scope.pages = [];
    $scope.announcements = [];
    $scope.announcement = {};

    $http.get("http://localhost:60163/groups/" + $scope.groupId, {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.group = data;
        });

    //get users
    $http.get("http://localhost:60163/groups/" + $scope.groupId + "/users", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.users = data;
        });

    //get announcements
    $http.get("http://localhost:60163/groups/" + $scope.groupId + "/announcements", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.announcements = data;
        });

    //get pages
    $http.get("http://localhost:60163/groups/" + $scope.groupId + "/pages", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.pages = data;
        });


    $scope.addAnnouncement = function () {
        $scope.announcement.group_id = $scope.groupId;

        $http.post("http://localhost:60163/announcements", $scope.announcement, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.announcements.push(data);
                $scope.announcement={};
            });
    };

    $scope.selectedPage = function (index) {
        var pageId = $scope.pages[index].Id;
        $location.path('/page/' + pageId);
    };

    $scope.seeUserProfile = function (index) {
        var id = $scope.users[index].Id;
        $location.path('/profile/' + id);
    };

});
userApp.controller('HomeworkController', function ($scope, $http, UserService, $sce, $location, $routeParams) {
    $scope.homeworks = [];
    $scope.homework = {};
    $scope.homeworkId = $routeParams.homework_id || "";
    $scope.answer = {};
    $scope.completed = false;
    $scope.professor = !UserService.getUser().Student;
    $scope.submitted_homework = [];
    $scope.grade = {};

    $http.get("http://localhost:60163/homeworks/myhomeworks", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.homeworks = data;
        }).error(function () {
        console.log("Eroare get teme!")
    });

    var getSubmittedHomework =function(){
        $http.get("http://localhost:60163/homeworks/" + $scope.homeworkId + "/submitted", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.submitted_homework = data;

            }).error(function () {
            console.log("Eroare get teme!")
        });
    }

    if (UserService.getUser().Student) {
        $http.get("http://localhost:60163/userHomeworks/" + $scope.homeworkId, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.answer = data;
                if (data.length != 0) {
                    $scope.completed = true;
                    $scope.answer.Answer = $sce.trustAsHtml(data.Answer);
                }
            }).error(function () {
            console.log("Eroare get teme!")
        });

        $scope.submitHomework = function () {
            var homework = {};
            homework.homework_id = $scope.homeworkId;
            homework.Answer = $('.text-editor').trumbowyg('html');

            $http.post("http://localhost:60163/userHomeworks", homework, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.completed = true;
                    $scope.answer = data;
                })
                .error(function (data, status) {
                    if (status == 400)
                        alert(data.Message);
                });
        }

    }
    else {
        getSubmittedHomework();

        $scope.addHomework = function () {
            $scope.homework.module_id = $routeParams.module_id;
            $http.post("http://localhost:60163/homeworks", $scope.homework, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $location.path('/homework/' + data.Id);
                })
                .error(function () {
                    console.log("Eroare post hw!")
                });
        };

        $scope.gradeHomework = function (index) {
            $scope.grade.user_id = $scope.submitted_homework[index].user_id;
            $scope.grade.homework_id = $scope.submitted_homework[index].homework_id;

            $http.post("http://localhost:60163/grades", $scope.grade, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.grade.Value="";
                    getSubmittedHomework();
                });

        };

        $scope.editHomework = function () {
            var homework = {
                Id: $scope.homework.Id,
                Name: $scope.homework.Name,
                Description: $scope.homework.Description,
                Pondere: $scope.homework.Pondere,
                End_date: $scope.homework.End_date,
                module_id: $scope.homework.module_id
            };
            $http.put("http://localhost:60163/homeworks/" + $scope.homeworkId, homework, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $location.path('/homework/' + $scope.homeworkId);
                })
                .error(function () {
                    console.log("Eroare post test!")
                });
        };

        $scope.editHomeworkPath = function () {
            $location.path('/edit-homework/' + $scope.homeworkId);

        };

        $scope.gradeHomeworkPath = function () {
            $location.path('/grade-homework/' + $scope.homeworkId);

        };
    }

    if ($routeParams.homework_id) {
        $http.get("http://localhost:60163/homeworks/" + $scope.homeworkId, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.homework = data;
                $scope.homework.Answer = $sce.trustAsHtml(data.Answer);
            }).error(function () {
            console.log("Eroare get tema!")
        });


    }

    $scope.selectedHomework = function (index) {
        var homeworkId = $scope.homeworks[index].Id;
        $location.path('/homework/' + homeworkId);
    };

});

userApp.controller('MessagesController', function ($scope, $http, $routeParams, $location, UserService) {
    $scope.conversations = [];
    $scope.old_conversations = [];
    $scope.conversation = {};
    $scope.messages = [];
    $scope.message = {};
    $scope.convId = $routeParams.conversation_id || "";
    $scope.convIndex = 0;
    $scope.users = [];

    $http.get("http://localhost:60163/users", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.users = data;
        });

    $http.get("http://localhost:60163/userConversations", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.conversations = data;
            $scope.old_conversations = data;
            if (!$scope.convId) {
                $location.path('/messages/' + $scope.conversations[0].Id);
            }

            for (var i = 0; i < data.length; i++) {
                if ($scope.conversations[i].Id == $scope.convId) {
                    $scope.convIndex = i;
                    break;
                }
            }
        })
        .error(function () {
            console.log("Eroare get conv!")
        });


    $http.get("http://localhost:60163/conversations/" + $scope.convId + "/messages", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.messages = data;
        })
        .error(function () {
            console.log("Eroare get conv!")
        });

    $scope.isMe = function (username) {
        if (username == UserService.getUser().Username)
            return true;
        return false;
    };

    $scope.sendMessage = function () {
        if (!$scope.message.Text) {
            return;
        }
        $scope.message.Date = new Date();
        $scope.message.userConversation_id = $scope.convId;
        $http.post("http://localhost:60163/messages", $scope.message, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.messages.push(data);
                $scope.message = {};
            })
            .error(function (data, status) {
                alert("Mesajul nu a fost trimis");
            });
    };

    $scope.addConversation = function () {
        var conversation = {Name: $scope.conversation.Name};
        $http.post("http://localhost:60163/conversations", conversation, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                var userconv1 = {user_id: UserService.getUser().Id, conversation_id: data.Id}
                $http.post("http://localhost:60163/userConversations", userconv1, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                });
                var user_id = $scope.users[$scope.conversation.user].Id;
                var username = $scope.users[$scope.conversation.user].Username;
                var userconv2 = {user_id: parseInt(user_id), conversation_id: data.Id}
                $http.post("http://localhost:60163/userConversations", userconv2, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                });
                $scope.conversations.push({
                    Name: data.Name,
                    Id: data.Id,
                    Users: [{Name: UserService.getUser().Username}, {Name: username}]
                });
            });
    };


    $scope.deleteConversation = function () {

        $http.delete("http://localhost:60163/conversations/" + $scope.convId, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
                $location.path('/messages');
            });
    };


    $scope.changeConversationName = function () {
        var conversation = {Id: $scope.convId, Name: $scope.conversation.Name};
        $http.put("http://localhost:60163/conversations/" + $scope.convId, conversation, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.conversations[$scope.convIndex].Name = $scope.conversation.Name;
            });
    };


    $scope.addUserToConversation = function () {
        var user_id = $scope.users[$scope.conversation.user].Id;
        var username = $scope.users[$scope.conversation.user].Username;
        var userconv = {user_id: parseInt(user_id), conversation_id: $scope.convId};
        $http.post("http://localhost:60163/userConversations", userconv, {
            headers: {
                "Token": UserService.getToken()
            }
        }).success(function (data) {
            $scope.conversations[$scope.convIndex].Users.push({Name: username});
        });
    };

    $scope.findConversation = function () {
        var new_conversations = [];
        var name = $scope.conversation.Name_search;

        if (name == null || name == "") {
            $scope.conversations = $scope.old_conversations.slice();
            console.log($scope.conversations);
        }
        else {
            for (var i = 0; i < $scope.old_conversations.length; i++) {
                if ($scope.old_conversations[i].Name.contains(name)) {
                    new_conversations.push($scope.old_conversations[i]);
                }
            }
            $scope.conversations = new_conversations;
        }
    };

    $scope.isCurrent = function (index) {
        return index == $scope.convIndex;
    };

    $scope.selectedConversation = function (index) {
        var convId = $scope.conversations[index].Id;
        $location.path('/messages/' + convId);
    }
});

userApp.controller('NotificationsController', function (UserService, $scope, $http, $routeParams) {
    $scope.notifications = {};

    $http.get("http://localhost:60163/userNotifications/mynotif", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.notifications = data;
        })
        .error(function () {
        });

    $scope.markAsSeen=function(index){
        var notId= $scope.notifications[index].Id;

        $http.get("http://localhost:60163/userNotifications/"+ notId +"/seen", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.notifications.splice(index,1);
            })
            .error(function () {
            });

    }

});
userApp.controller('PagesController', function (UserService, $scope, $http, $routeParams, $location, $sce) {
    $scope.page = {};
    $scope.pageId = $routeParams.page_id || "";

    if ($routeParams.page_id) {
        $http.get("http://localhost:60163/api/pages/" + $scope.pageId, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.page = data;
                $('.text-editor').trumbowyg('html', $scope.page.Text);
            })
            .error(function () {
                console.log("Eroare get page!")
            });
    }

    $scope.addPage = function () {
        $scope.page.user_id = UserService.getUser().Id;
        $scope.page.group_id = UserService.getUser().group_id;
        $scope.page.Text = $('.text-editor').trumbowyg('html');

        $http.post("http://localhost:60163/api/pages", $scope.page, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $location.path('/group/pages');
            })
            .error(function (data, status) {
                alert("Pagina nu a fost adaugata! " + "(Error " + status + ")");
            });
    };

    $scope.editPermission = function () {
        if ($scope.page.user_id == UserService.getUser().Id) {
            return true;
        }
        return false;
    }

    $scope.editPage = function () {
        $scope.page.user_id = UserService.getUser().Id;
        $scope.page.group_id = UserService.getUser().group_id;
        $scope.page.User = null;

        $scope.page.Text = $('.text-editor').trumbowyg('html');

        $http.put("http://localhost:60163/api/pages/" + $scope.pageId, $scope.page, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
                $location.path('/page/' + $scope.pageId);
            })
            .error(function () {
                alert("Pagina nu s-a putut modifica!")
            });
    }

    $scope.openEdit = function () {
        $location.path('/edit-page/' + $scope.pageId);
    }

});

userApp.controller('ProfileController', function (UserService, $scope, $http, $routeParams) {
    $scope.user = {};
    $scope.oldPassword = "";
    $scope.newPassword = "";
    $scope.renewPassword = "";
    $scope.userId = $routeParams.user_id || "";

    if ($routeParams.user_id) {
        $http.get("http://localhost:60163/users/" + $scope.userId, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.user = data;
            })
            .error(function () {
                console.log("Eroare get conv!")
            });
    }
    else {
        $http.get("http://localhost:60163/users/user", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.user = data;
                if (data.group_id == null) {
                    $scope.user.group_name = "-";
                }
            })
            .error(function () {
                console.log("Eroare get conv!")
            });
    }


    $scope.resetPassword = function () {
        if ($scope.newPassword === $scope.renewPassword) {
            $http.get("http://localhost:60163/users/verifyPass/" + $scope.oldPassword,
                {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function () {
                    $http.post("http://localhost:60163/users/resetPass/" + $scope.newPassword, {}, {
                            headers: {
                                "Token": UserService.getToken()
                            }
                        })
                        .success(function () {
                            alert("Parola a fost schimbata!")
                        })
                        .error(function () {
                            console.log("Parola nu s-a putut modifica!")
                        });
                })
                .error(function () {
                    console.log("Parola veche nu este corecta!")
                });

        } else {
            alert("Cele doua parole nu sunt la fel!");
        }


    };

});
userApp.controller('TestsController', function (UserService, $scope, $http, $location, $routeParams) {
    $scope.tests = [];
    $scope.test = {};

    $http.get("http://localhost:60163/tests/mytests", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.tests = data;
        }).error(function () {
        console.log("Eroare get test!")
    });

    $scope.selectedTest = function (index) {
        var testId = $scope.tests[index].Id;
        $location.path('/test/' + testId);
    };

    $scope.addTest = function (index) {
        var testId = $scope.tests[index].Id;
        $location.path('/test/' + testId);
    };


});

//-------------------------------------------------------------------------------------------

userApp.controller('TestController', function ($scope, $location, $http, UserService, $routeParams) {
    $scope.test = {};
    $scope.testId = $routeParams.test_id;
    $scope.grade = {};
    $scope.questions = [];
    $scope.question = {};
    $scope.completed = false;
    $scope.professor = !UserService.getUser().Student;
    $scope.choice = {};
    $scope.questions = [];

    $http.get("http://localhost:60163/tests/" + $scope.testId, {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.test = data;
        });

    if (UserService.getUser().Student) {
        $http.get("http://localhost:60163/grades/tests/" + $scope.testId + "/getGrade", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.grade = data.Grade;
                if (data.Grade != -1)
                    $scope.completed = true;
            });


        $scope.startTest = function () {
            $location.path('/test/' + $scope.testId + '/question/1');
        };
    }
    else {
        $scope.addTest = function () {
            $scope.test.module_id = $routeParams.module_id;
            $http.post("http://localhost:60163/tests", $scope.test, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.addQuestions(data.Id);
                    $location.path('/test/' + data.Id);
                })
                .error(function () {
                    console.log("Eroare post test!")
                });
        };

        $scope.addQuestionChoices = function (question, choices) {
            $http.post("http://localhost:60163/questions", question, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    for (var j = 0; j < choices.length; j++) {
                        var choice = {};
                        choice.Description = choices[j].Description;
                        choice.Correct = choices[j].Correct;
                        choice.question_id = data.Id;
                        $http.post("http://localhost:60163/api/AnswerOptions", choice, {
                            headers: {
                                "Token": UserService.getToken()
                            }
                        });
                    }
                });
        };

        $scope.addQuestions = function (testId) {
            for (var i = 0; i < $scope.questions.length; i++) {
                var question = {};
                question.Description = $scope.questions[i].Description;
                question.Pondere = $scope.questions[i].Pondere;
                question.test_id = testId;
                $scope.addQuestionChoices(question, $scope.questions[i].choices);
            }
        };

        $scope.editTest = function () {
            var test = {
                "Id": $scope.test.Id,
                "Name": $scope.test.Name,
                "Description": $scope.test.Description,
                "Due_date": $scope.test.Due_date,
                "Pondere": $scope.test.Pondere,
                "module_id": $scope.test.module_id
            }
            $http.put("http://localhost:60163/tests/" + $scope.testId, test, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.addQuestions($scope.testId);
                    $location.path('/test/' + $scope.testId);
                })
                .error(function () {
                    console.log("Eroare post test!")
                });
        };

        $scope.editTestPath = function () {
            $location.path('/edit-test/' + $scope.testId);

        };

        $scope.addQuestion = function () {
            if ($scope.question.Description == null || $scope.question.Pondere == null || $scope.question.choices.length == 0) {
                alert("Toate campurile sunt obligatorii!")
                return;
            }
            $scope.questions.push($scope.question);
            $scope.question = {};
        };

        $scope.newQuestion = function () {
            $scope.question.choices = [];
        };

        $scope.addChoice = function () {
            if ($scope.choice.Description == null) {
                alert("Descrierea este obligatorie!")
                return;
            }
            var newChoice = {Description: $scope.choice.Description, Correct: $scope.choice.Correct};
            $scope.question.choices.push(newChoice);
        };


        $http.get("http://localhost:60163/tests/" + $scope.testId + "/questions", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.questions = data;

                for (var i = 0; i < $scope.questions.length; i++) {
                    $scope.getAnswers(i);
                }
            });

        $scope.getAnswers = function (i) {
            $http.get("http://localhost:60163/questions/" + $scope.questions[i].Id + "/answers", {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.questions[i].Answers = data;
                    console.log($scope.questions[i].Answers);
                });
        }
    }


});
//-------------------------------------------------------------------------------------------

userApp.controller('QuestionsController', function ($scope, $http, UserService, $location, $routeParams) {
    $scope.test = {};
    $scope.testId = $routeParams.test_id;
    $scope.questionNr = $routeParams.question_nr;
    $scope.question = {};
    $scope.questions = [];
    $scope.answer = {};
    $scope.choices = [];
    $scope.endTest = false;
    $scope.questionId = "";
    $scope.answers = [];
    $scope.choice = new Array(4);

    $scope.addQuestion = function () {
        $scope.question.test_id = $scope.testId;
        console.log($scope.question);
        $http.post("http://localhost:60163/questions", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                for (var i = 0; i < 4; i++) {
                    $scope.choice[i].question_id = data.Id;
                    $scope.comment.Date = new Date();
                    $http.post("http://localhost:60163/api/AnswerOptions", $scope.choice[i], {
                            headers: {
                                "Token": UserService.getToken()
                            }
                        })
                        .success(function () {
                        })
                        .error(function () {
                            console.log("Eroare add comment!")
                        });
                }

            })
            .error(function () {
                console.log("Eroare add discussion!")
            });
    };


    $http.get("http://localhost:60163/tests/" + $scope.testId, {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.test = data;
        }).error(function () {
        console.log("Eroare get teste!")
    });


    $http.get("http://localhost:60163/tests/" + $scope.testId + "/questions", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.questions = data;
            $scope.questionId = $scope.questions[parseInt($scope.questionNr) - 1].Id;
            $scope.question = data[parseInt($scope.questionNr) - 1];

            if ($scope.questionNr >= $scope.questions.length) {
                $scope.endTest = true;
            }

            $http.get("http://localhost:60163/questions/" + $scope.questionId + "/answers", {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.choices = data;
                }).error(function () {
                console.log("Eroare get answers!")
            });

        }).error(function () {
        console.log("Eroare get intrebari!")
    });


    $scope.nextQuestion = function (data) {
        $scope.submitAnswers(data);
        $scope.questionNr = parseInt($scope.questionNr) + 1;
        $location.path('/test/' + $scope.testId + '/question/' + $scope.questionNr);
    };

    $scope.finishTest = function (data) {
        $scope.submitAnswers(data);
        $scope.getGrade();
        $scope.completed=true;
        $scope.questionNr = parseInt($scope.questionNr) + 1;
        $location.path('/tests');
    };

    $scope.getGrade = function () {
        $http.get("http://localhost:60163/grades/tests/" + $scope.testId + "/mygrade", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
            })
            .error(function () {
                console.log("Eroare put test!");
            });
    };


    $scope.submitAnswers = function (choices) {
        for (var i in choices) {
            if (choices[i].SELECTED == 'Y') {
                $scope.answer.user_Id = UserService.getUser().Id;
                $scope.answer.answerOptionId = choices[i].Id;

                $http.post("http://localhost:60163/userAnswers", $scope.answer, {
                        headers: {
                            "Token": UserService.getToken()
                        }
                    })
                    .success(function (data) {
                    })
                    .error(function () {
                        console.log("Eroare post answers!")
                    });
            }
        }
        console.log($scope.answers);
    };

});
