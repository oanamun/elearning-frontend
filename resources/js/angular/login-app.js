var loginApp = angular.module("loginApp", ['ngRoute', 'ngStorage']);

loginApp.config(function ($routeProvider) {
    $routeProvider
        .when('/login', {
            templateUrl: 'templates/login.html',
            controller: 'LoginController'
        })
        .when('/reset-password', {
            templateUrl: 'templates/forgot_password.html',
            controller: 'LoginController'
        })
        .otherwise({
            redirectTo: '/login'
        });
});

loginApp.factory('LoginService', function ($localStorage) {
    return {
        setUser: function (value) {
            $localStorage.user = value;
        },
        setToken: function (value) {
            $localStorage.token = value;
        }
    }
});


loginApp.controller('LoginController', function (LoginService, $scope, $http, $window) {
    $scope.username = "";
    $scope.password = "";
    $scope.loading = false;

    $scope.login = function () {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $scope.username + ':' + $scope.password;
        $scope.loading = true;
        $http.post("http://localhost:60163/api/login")
            .success(function (data, status, headers) {
                LoginService.setToken(headers('Token'));
                LoginService.setUser(data);
                if (data.Suspended) {
                    alert("Acest cont este suspendat!");
                }
                else if (data.Blocked) {
                    alert("Acest cont este blocat!");
                }
                else {
                    $scope.changePath(headers('Token'));
                }

            })
            .error(function () {
                $scope.loading = false;
                alert("Login-ul nu s-a facut cu succes!")
            });
    };

    $scope.changePath = function (token) {
        $http.get("http://localhost:60163/users/user/permissions", {
            headers: {
                "Token": token
            }
        }).success(function (data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].Role == "Admin") {
                    $window.location.href = '/elearning-frontend/admin.html';
                    return;
                }
            }
            $window.location.href = '/elearning-frontend/index.html';
        });

    };

});

loginApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown", function (e) {
            if (e.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter, {'e': e});
                });
                e.preventDefault();
            }
        });
    };
});

