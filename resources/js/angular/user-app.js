var userApp = angular.module("userApp", ['ngRoute', 'ngStorage', 'ngSanitize', 'luegg.directives']);

userApp.config(function ($routeProvider) {
    //$locationProvider.html5Mode(true);
    $routeProvider
        .when('/courses', {
            title: 'Cursuri',
            templateUrl: 'templates/courses.html',
            controller: 'CoursesController',
            resolve: {
                mess: function ($localStorage, $window) {
                    var t = $localStorage.token;
                    if (t === '') {
                        $window.location.href = '/elearning-frontend/login.html';
                    }
                }

            }
        })
        .when('/all-courses', {
            title: 'Cursuri',
            templateUrl: 'templates/register-course.html',
            controller: 'CoursesController'
        })
        .when('/course/:course_id', {
            title: 'Curs',
            templateUrl: 'templates/course.html',
            controller: 'CourseController'
        })
        .when('/course/:course_id/discussions', {
            title: 'Curs',
            templateUrl: 'templates/course-discussions.html',
            controller: 'CourseController'
        })
        .when('/course/:course_id/announcements', {
            title: 'Curs',
            templateUrl: 'templates/course-announcements.html',
            controller: 'CourseController'
        })
        .when('/course/:course_id/students', {
            title: 'Studenti',
            templateUrl: 'templates/course-students.html',
            controller: 'CourseController'
        })
        .when('/add-course', {
            title: 'Adauga curs',
            templateUrl: 'templates/add-course.html',
            controller: 'CoursesController'
        })
        .when('/module/:module_id', {
            title: 'Modul',
            templateUrl: 'templates/module.html',
            controller: 'ModuleController'
        })
        .when('/module/:module_id/teste', {
            title: 'Test',
            templateUrl: 'templates/module-test.html',
            controller: 'ModuleController'
        })
        .when('/module/:module_id/teme', {
            title: 'Tema',
            templateUrl: 'templates/module-homework.html',
            controller: 'ModuleController'
        })
        .when('/add-module/:course_id', {
            title: 'Adauga modul',
            templateUrl: 'templates/add-module.html',
            controller: 'CourseController'
        })
        .when('/edit-module/:module_id', {
            title: 'Editeaza modul',
            templateUrl: 'templates/edit-module.html',
            controller: 'ModuleController'
        })
        .when('/add-test/:module_id', {
            title: 'Adauga test',
            templateUrl: 'templates/add-test.html',
            controller: 'TestController'
        })
        .when('/edit-test/:test_id', {
            title: 'Editeaza test',
            templateUrl: 'templates/edit-test.html',
            controller: 'TestController'
        })
        .when('/add-homework/:module_id', {
            title: 'Adauga tema',
            templateUrl: 'templates/add-homework.html',
            controller: 'HomeworkController'
        })
        .when('/edit-homework/:homework_id', {
            title: 'Editeaza tema',
            templateUrl: 'templates/edit-homework.html',
            controller: 'HomeworkController'
        })
        .when('/homework/:homework_id', {
            title: 'Tema',
            templateUrl: 'templates/homework-page.html',
            controller: 'HomeworkController'
        })
        .when('/grade-homework/:homework_id', {
            title: 'Teme',
            templateUrl: 'templates/grade-homework.html',
            controller: 'HomeworkController'
        })
        .when('/forum', {
            title: 'Forum',
            templateUrl: 'templates/forum.html',
            controller: 'DiscussionsController'
        })
        .when('/discussion/:discussion_id', {
            title: 'Discutie',
            templateUrl: 'templates/discussion.html',
            controller: 'DiscussionController'
        })
        .when('/add-discussion', {
            title: 'Adauga discutie',
            templateUrl: 'templates/add-discussion.html',
            controller: 'DiscussionsController'
        })
        .when('/calendar', {
            title: 'Calendar',
            templateUrl: 'templates/calendar.html',
            controller: 'CalendarController'
        })
        .when('/messages', {
            title: 'Mesaje',
            templateUrl: 'templates/messages.html',
            controller: 'MessagesController'
        })
        .when('/messages/:conversation_id', {
            title: 'Mesaje',
            templateUrl: 'templates/messages.html',
            controller: 'MessagesController'
        })
        .when('/add-conversation', {
            title: 'Mesaje',
            templateUrl: 'templates/add-conversation.html',
            controller: 'ConversationController'
        })
        .when('/tests', {
            title: 'Teste',
            templateUrl: 'templates/tests.html',
            controller: 'TestsController'
        })
        .when('/homework', {
            title: 'Teme',
            templateUrl: 'templates/homework.html',
            controller: 'HomeworkController'
        })
        .when('/grades', {
            title: 'Note',
            templateUrl: 'templates/grades.html',
            controller: 'GradesController'
        })
        .when('/grades/:course_id', {
            title: 'Note curs',
            templateUrl: 'templates/course-grades.html',
            controller: 'StudentGradesController'
        })
        .when('/grades-final/:course_id', {
            title: 'Note',
            templateUrl: 'templates/grades-final.html',
            controller: 'TeacherGradesController'
        })
        .when('/grades-course/:course_id', {
            title: 'Note',
            templateUrl: 'templates/grades-course.html',
            controller: 'TeacherGradesController'
        })
        .when('/grades/:course_id/user/:user_id', {
            title: 'Note',
            templateUrl: 'templates/grades-user.html',
            controller: 'TeacherGradesController'
        })
        .when('/grades-course/:course_id/:elem_type/:elem_id', {
            title: 'Note',
            templateUrl: 'templates/grades-element.html',
            controller: 'TeacherGradesController'
        })
        .when('/group', {
            title: 'Grupa',
            templateUrl: 'templates/group.html',
            controller: 'GroupController'
        })
        .when('/group/pages', {
            title: 'Grupa',
            templateUrl: 'templates/group-pages.html',
            controller: 'GroupController'
        })
        .when('/page/:page_id', {
            title: 'Pagina',
            templateUrl: 'templates/page.html',
            controller: 'PagesController'
        })
        .when('/edit-page/:page_id', {
            title: 'Pagina',
            templateUrl: 'templates/edit-page.html',
            controller: 'PagesController'
        })
        .when('/group/announcements', {
            title: 'Grupa',
            templateUrl: 'templates/group-announcements.html',
            controller: 'GroupController'
        })
        .when('/add-page', {
            title: 'Adauga pagina',
            templateUrl: 'templates/add-page.html',
            controller: 'PagesController'
        })
        .when('/tests', {
            title: 'Teste',
            templateUrl: 'templates/tests.html',
            controller: 'TestsController'
        })
        .when('/test/:test_id', {
            title: 'Test',
            templateUrl: 'templates/test.html',
            controller: 'TestController'
        })
        .when('/test/:test_id/question/:question_nr', {
            title: 'Test',
            templateUrl: 'templates/question.html',
            controller: 'QuestionsController'
        })
        .when('/help', {
            title: 'Help',
            templateUrl: 'templates/help.html',
            controller: ''
        })
        .when('/profile', {
            title: 'Profil',
            templateUrl: 'templates/profile.html',
            controller: 'ProfileController'
        })
        .when('/profile/:user_id', {
            title: 'Profil',
            templateUrl: 'templates/user-profile.html',
            controller: 'ProfileController'
        })
        .when('/notifications', {
            title: 'Notificari',
            templateUrl: 'templates/notifications.html',
            controller: 'NotificationsController'
        })
        .when('/videoconf', {
            title: 'Videoconferinta',
            templateUrl: 'templates/videoconf.html'
        })
        .otherwise({
            redirectTo: '/courses'
        });
});

userApp.run(['$rootScope', '$route', function ($rootScope, $route) {
    $rootScope.$on('$routeChangeSuccess', function () {
        document.title = $route.current.title;
    });
}]);


userApp.factory('UserService', function ($localStorage) {
    return {
        setUser: function (value) {
            $localStorage.user = value;
        },
        setToken: function (value) {
            $localStorage.token = value;
        },
        getUser: function () {
            return $localStorage.user;
        },
        getToken: function () {
            return $localStorage.token;
        }
    }
});


userApp.controller('MainController', function (UserService, $scope, $window) {
    $scope.user = UserService.getUser();

    $scope.logout = function () {
        UserService.setToken('');
        UserService.setUser('');
    }

});


userApp.directive('loadCalendar', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element).fullCalendar(scope.$eval({
                theme: true,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                editable: true,
                eventLimit: true,
                timeFormat: 'H:mm'
            }));
        }
    };
});


userApp.directive('textEditor', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element).trumbowyg(scope.$eval({
                fullscreenable: true,
                btns: ['viewHTML',
                    '|', 'formatting',
                    '|', 'btnGrp-design',
                    '|', 'link',
                    '|', 'insertImage',
                    '|', 'btnGrp-justify',
                    '|', 'btnGrp-lists',
                    '|', 'horizontalRule']
            }));
        }
    };
});


userApp.directive('slimScroll', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element).slimScroll({
                height: '300px',
                color: '#00D6B2'
            });
        }
    };
});


userApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown", function (e) {
            if (e.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter, {'e': e});
                });
                e.preventDefault();
            }
        });
    };
});

