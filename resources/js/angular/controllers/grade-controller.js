userApp.controller('TeacherGradesController', function (UserService, $scope, $http, $location, $routeParams) {
        $scope.grades = [];
        $scope.courseId = $routeParams.course_id || "";
        $scope.professor = !UserService.getUser().Student;
        $scope.elements = [];
        $scope.userId = $routeParams.user_id || "";
        $scope.elemId = $routeParams.elem_id || "";
        $scope.elemType = $routeParams.elem_type || "";
        $scope.courseName = "";

        var getGrades = function () {
            $http.get("http://localhost:60163/grades/" + $scope.courseId + "/students/finals", {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.grades = data;
                })
                .error(function () {
                    console.log("Eroare get courses!")
                });

        };

        if ($scope.courseId) {
            $http.get("http://localhost:60163/courses/" + $scope.courseId, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.courseName = data.Name;
                });

            if ($scope.userId) {
                $http.get("http://localhost:60163/grades/" + $scope.courseId + "/" + $scope.userId + "/details", {
                        headers: {
                            "Token": UserService.getToken()
                        }
                    })
                    .success(function (data) {
                        $scope.grades = data;
                    });
            }
            else if ($scope.elemId) {
                if ($scope.elemType == "Tema") {
                    $http.get("http://localhost:60163/grades/homeworks/" + $scope.elemId, {
                            headers: {
                                "Token": UserService.getToken()
                            }
                        })
                        .success(function (data) {
                            $scope.grades = data;
                        })

                }
                else if ($scope.elemType == "Test") {
                    $http.get("http://localhost:60163/grades/tests/" + $scope.elemId, {
                            headers: {
                                "Token": UserService.getToken()
                            }
                        })
                        .success(function (data) {
                            $scope.grades = data;
                        })
                }
                else if ($scope.elemType == "Discutie") {
                    $http.get("http://localhost:60163/grades/discussions/" + $scope.elemId, {
                            headers: {
                                "Token": UserService.getToken()
                            }
                        })
                        .success(function (data) {
                            $scope.grades = data;
                        })
                }

            }
            else {
               getGrades();

                $http.get("http://localhost:60163/courses/" + $scope.courseId + "/elements", {
                        headers: {
                            "Token": UserService.getToken()
                        }
                    })
                    .success(function (data) {
                        $scope.elements = data;
                    })
                    .error(function () {
                        console.log("Eroare get courses!")
                    });

            }
        }

        $scope.getFinalGrades = function () {
            $http.get("http://localhost:60163/courses/" + $scope.courseId + "/calculateFinals", {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    getGrades();
                });
        };

        $scope.userGradesPage = function (index) {
            var userId = $scope.grades[index].user_id;
            $location.path('/grades/' + $scope.courseId + "/user/" + userId);
        };
        $scope.courseFinalPage = function () {
            $location.path('/grades-final/' + $scope.courseId);
        };
        $scope.courseGradesPage = function () {
            $location.path('/grades-course/' + $scope.courseId);
        };

        $scope.courseElementPage = function (index) {
            var elemId = $scope.elements[index].Id;
            var elemType = $scope.elements[index].Type;
            $location.path('/grades-course/' + $scope.courseId + "/" + elemType + "/" + elemId);
        };
    }
);

userApp.controller('StudentGradesController', function (UserService, $scope, $http, $location, $routeParams) {
        $scope.grades = [];
        $scope.courseId = $routeParams.course_id || "";


        $http.get("http://localhost:60163/grades/finals", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.grades = data;
            })
            .error(function () {
                console.log("Eroare get modul!")
            });

        if ($scope.courseId) {
            $http.get("http://localhost:60163/courses/" + $scope.courseId + "/grades", {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.grades = data;
                })
                .error(function () {
                    console.log("Eroare get grade!")
                });
        }
    }
);


userApp.controller('GradesController', function (UserService, $scope, $http, $location, $routeParams) {
        $scope.grades = [];
        $scope.professor = !UserService.getUser().Student;

        if (UserService.getUser().Student) {
            $http.get("http://localhost:60163/grades/finals", {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.grades = data;
                })
                .error(function () {
                    console.log("Eroare get modul!")
                });

            if ($scope.courseId) {
                $http.get("http://localhost:60163/courses/" + $scope.courseId + "/grades", {
                        headers: {
                            "Token": UserService.getToken()
                        }
                    })
                    .success(function (data) {
                        $scope.grades = data;
                    })
                    .error(function () {
                        console.log("Eroare get grade!")
                    });
            }
            $scope.courseGradesPage = function (index) {
                var courseId = $scope.grades[index].course_id;
                $location.path('/grades/' + courseId);
            };
        } else {
            $http.get("http://localhost:60163/courses/mycourses", {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.courses = data;
                })
                .error(function () {
                    console.log("Eroare get courses!")
                });
            $scope.profGradesPage = function (index) {
                var courseId = $scope.courses[index].Id;
                $location.path('/grades-final/' + courseId);
            };
        }
    }
);