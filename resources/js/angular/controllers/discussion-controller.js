userApp.controller('DiscussionsController', function ($scope, $http, UserService, $location, $routeParams) {
    $scope.group_discussions = [];
    $scope.discussion = {}
    $scope.comment = {};

    $http.get("http://localhost:60163/forum", {
        headers: {
            "Token": UserService.getToken()
        }
    }).success(function (data) {
            $scope.group_discussions = data;
        })
        .error(function () {
            console.log("Eroare get discutii grup!")
        });


    $scope.addDiscussion = function () {
        if ($routeParams.discussion_id) {
            $scope.discussion.course_id = $routeParams.discussion_id;
        }

        $http.post("http://localhost:60163/forum", $scope.discussion, {
            headers: {
                "Token": UserService.getToken()
            }
        }).success(function (data) {
                $scope.group_discussions.push(data);
                $scope.discussion = {};
            })
            .error(function () {
                console.log("Eroare add discussion!")
            });
    };

    // go to discussion page
    $scope.selectedDiscussion = function (index) {
        var discussionId = $scope.group_discussions[index].Id;
        $location.path('/discussion/' + discussionId);
    };

});

//-------------------------------------------------------------------------------------------

userApp.controller('DiscussionController', function ($scope, $http,$location, UserService, $routeParams) {
    $scope.comment = {};
    $scope.discussion = {};
    $scope.comments = [];
    $scope.discussionId = $routeParams.discussion_id;
    $scope.professor = false;
    $scope.users = [];
    $scope.user ="";
    $scope.grade = {};
    $scope.currentPage = 0;
    $scope.pageSize = 5;

    $http.get("http://localhost:60163/forum/" + $scope.discussionId, {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.discussion = data;
            $scope.getPermission();
        });

    $http.get("http://localhost:60163/forum/" + $scope.discussionId + "/comments", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.comments = data;
        })
        .error(function () {
            console.log("Eroare get comments!")
        });

    if(!UserService.getUser().Student) {
        $http.get("http://localhost:60163/forum/" + $scope.discussionId + "/participants", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.users = data;
            })
            .error(function () {
                console.log("Eroare get comments!")
            });
    }

    $scope.addComment = function () {
        $scope.comment.discussion_id = $scope.discussionId;
        $scope.comment.Date = new Date();
        $scope.comment.user_id = UserService.getUser().Id;
        $http.post("http://localhost:60163/api/comments", $scope.comment, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                data.user_name = UserService.getUser().Username;
                data.group_name = UserService.getUser().group_name;
                $scope.comments.push(data);
            }).error(function () {
            console.log("Eroare add comment!")
        });
        $scope.comment = "";
    };

    $scope.addGrade = function () {
        $scope.grade.user_id = $scope.user;
        $scope.grade.discussion_id = $scope.discussionId;

        $http.post("http://localhost:60163/grades", $scope.grade, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
            });
    };

    $scope.deleteDiscussion = function () {
        var courseId = $scope.discussion.course_id;

        $http.delete("http://localhost:60163/forum/" + $scope.discussionId, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
                $location.path('/course/' + courseId +'/discussions');
            });

    };

    $scope.getPermission = function () {
        $http.get("http://localhost:60163/courses/mycourses/" + $scope.discussion.course_id + "/permission", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                if (data.Role === "Profesor" || data.Role === "Asistent") {
                    $scope.professor = true;
                }

            })
            .error(function () {
                console.log("Eroare get permissions!")
            });
    };

    $scope.numberOfPages=function(){
        return Math.ceil($scope.comments.length/$scope.pageSize);
    }
});

userApp.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});