userApp.controller('HomeworkController', function ($scope, $http, UserService, $sce, $location, $routeParams) {
    $scope.homeworks = [];
    $scope.homework = {};
    $scope.homeworkId = $routeParams.homework_id || "";
    $scope.answer = {};
    $scope.completed = false;
    $scope.professor = !UserService.getUser().Student;
    $scope.submitted_homework = [];
    $scope.grade = {};

    $http.get("http://localhost:60163/homeworks/myhomeworks", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.homeworks = data;
        }).error(function () {
        console.log("Eroare get teme!")
    });

    var getSubmittedHomework =function(){
        $http.get("http://localhost:60163/homeworks/" + $scope.homeworkId + "/submitted", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.submitted_homework = data;

            }).error(function () {
            console.log("Eroare get teme!")
        });
    }

    if (UserService.getUser().Student) {
        $http.get("http://localhost:60163/userHomeworks/" + $scope.homeworkId, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.answer = data;
                if (data.length != 0) {
                    $scope.completed = true;
                    $scope.answer.Answer = $sce.trustAsHtml(data.Answer);
                }
            }).error(function () {
            console.log("Eroare get teme!")
        });

        $scope.submitHomework = function () {
            var homework = {};
            homework.homework_id = $scope.homeworkId;
            homework.Answer = $('.text-editor').trumbowyg('html');

            $http.post("http://localhost:60163/userHomeworks", homework, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.completed = true;
                    $scope.answer = data;
                })
                .error(function (data, status) {
                    if (status == 400)
                        alert(data.Message);
                });
        }

    }
    else {
        getSubmittedHomework();

        $scope.addHomework = function () {
            $scope.homework.module_id = $routeParams.module_id;
            $http.post("http://localhost:60163/homeworks", $scope.homework, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $location.path('/homework/' + data.Id);
                })
                .error(function () {
                    console.log("Eroare post hw!")
                });
        };

        $scope.gradeHomework = function (index) {
            $scope.grade.user_id = $scope.submitted_homework[index].user_id;
            $scope.grade.homework_id = $scope.submitted_homework[index].homework_id;

            $http.post("http://localhost:60163/grades", $scope.grade, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.grade.Value="";
                    getSubmittedHomework();
                });

        };

        $scope.editHomework = function () {
            var homework = {
                Id: $scope.homework.Id,
                Name: $scope.homework.Name,
                Description: $scope.homework.Description,
                Pondere: $scope.homework.Pondere,
                End_date: $scope.homework.End_date,
                module_id: $scope.homework.module_id
            };
            $http.put("http://localhost:60163/homeworks/" + $scope.homeworkId, homework, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $location.path('/homework/' + $scope.homeworkId);
                })
                .error(function () {
                    console.log("Eroare post test!")
                });
        };

        $scope.editHomeworkPath = function () {
            $location.path('/edit-homework/' + $scope.homeworkId);

        };

        $scope.gradeHomeworkPath = function () {
            $location.path('/grade-homework/' + $scope.homeworkId);

        };
    }

    if ($routeParams.homework_id) {
        $http.get("http://localhost:60163/homeworks/" + $scope.homeworkId, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.homework = data;
                $scope.homework.Answer = $sce.trustAsHtml(data.Answer);
            }).error(function () {
            console.log("Eroare get tema!")
        });


    }

    $scope.selectedHomework = function (index) {
        var homeworkId = $scope.homeworks[index].Id;
        $location.path('/homework/' + homeworkId);
    };

});
