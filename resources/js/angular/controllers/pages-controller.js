userApp.controller('PagesController', function (UserService, $scope, $http, $routeParams, $location, $sce) {
    $scope.page = {};
    $scope.pageId = $routeParams.page_id || "";

    if ($routeParams.page_id) {
        $http.get("http://localhost:60163/api/pages/" + $scope.pageId, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.page = data;
                $('.text-editor').trumbowyg('html', $scope.page.Text);
            })
            .error(function () {
                console.log("Eroare get page!")
            });
    }

    $scope.addPage = function () {
        $scope.page.user_id = UserService.getUser().Id;
        $scope.page.group_id = UserService.getUser().group_id;
        $scope.page.Text = $('.text-editor').trumbowyg('html');

        $http.post("http://localhost:60163/api/pages", $scope.page, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $location.path('/group/pages');
            })
            .error(function (data, status) {
                alert("Pagina nu a fost adaugata! " + "(Error " + status + ")");
            });
    };

    $scope.editPermission = function () {
        if ($scope.page.user_id == UserService.getUser().Id) {
            return true;
        }
        return false;
    }

    $scope.editPage = function () {
        $scope.page.user_id = UserService.getUser().Id;
        $scope.page.group_id = UserService.getUser().group_id;
        $scope.page.User = null;

        $scope.page.Text = $('.text-editor').trumbowyg('html');

        $http.put("http://localhost:60163/api/pages/" + $scope.pageId, $scope.page, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
                $location.path('/page/' + $scope.pageId);
            })
            .error(function () {
                alert("Pagina nu s-a putut modifica!")
            });
    }

    $scope.openEdit = function () {
        $location.path('/edit-page/' + $scope.pageId);
    }

});
