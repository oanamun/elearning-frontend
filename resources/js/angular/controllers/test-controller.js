userApp.controller('TestsController', function (UserService, $scope, $http, $location, $routeParams) {
    $scope.tests = [];
    $scope.test = {};

    $http.get("http://localhost:60163/tests/mytests", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.tests = data;
        }).error(function () {
        console.log("Eroare get test!")
    });

    $scope.selectedTest = function (index) {
        var testId = $scope.tests[index].Id;
        $location.path('/test/' + testId);
    };

    $scope.addTest = function (index) {
        var testId = $scope.tests[index].Id;
        $location.path('/test/' + testId);
    };


});

//-------------------------------------------------------------------------------------------

userApp.controller('TestController', function ($scope, $location, $http, UserService, $routeParams) {
    $scope.test = {};
    $scope.testId = $routeParams.test_id;
    $scope.grade = {};
    $scope.questions = [];
    $scope.question = {};
    $scope.completed = false;
    $scope.professor = !UserService.getUser().Student;
    $scope.choice = {};
    $scope.questions = [];

    $http.get("http://localhost:60163/tests/" + $scope.testId, {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.test = data;
        });

    if (UserService.getUser().Student) {
        $http.get("http://localhost:60163/grades/tests/" + $scope.testId + "/getGrade", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.grade = data.Grade;
                if (data.Grade != -1)
                    $scope.completed = true;
            });


        $scope.startTest = function () {
            $location.path('/test/' + $scope.testId + '/question/1');
        };
    }
    else {
        $scope.addTest = function () {
            $scope.test.module_id = $routeParams.module_id;
            $http.post("http://localhost:60163/tests", $scope.test, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.addQuestions(data.Id);
                    $location.path('/test/' + data.Id);
                })
                .error(function () {
                    console.log("Eroare post test!")
                });
        };

        $scope.addQuestionChoices = function (question, choices) {
            $http.post("http://localhost:60163/questions", question, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    for (var j = 0; j < choices.length; j++) {
                        var choice = {};
                        choice.Description = choices[j].Description;
                        choice.Correct = choices[j].Correct;
                        choice.question_id = data.Id;
                        $http.post("http://localhost:60163/api/AnswerOptions", choice, {
                            headers: {
                                "Token": UserService.getToken()
                            }
                        });
                    }
                });
        };

        $scope.addQuestions = function (testId) {
            for (var i = 0; i < $scope.questions.length; i++) {
                var question = {};
                question.Description = $scope.questions[i].Description;
                question.Pondere = $scope.questions[i].Pondere;
                question.test_id = testId;
                $scope.addQuestionChoices(question, $scope.questions[i].choices);
            }
        };

        $scope.editTest = function () {
            var test = {
                "Id": $scope.test.Id,
                "Name": $scope.test.Name,
                "Description": $scope.test.Description,
                "Due_date": $scope.test.Due_date,
                "Pondere": $scope.test.Pondere,
                "module_id": $scope.test.module_id
            }
            $http.put("http://localhost:60163/tests/" + $scope.testId, test, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.addQuestions($scope.testId);
                    $location.path('/test/' + $scope.testId);
                })
                .error(function () {
                    console.log("Eroare post test!")
                });
        };

        $scope.editTestPath = function () {
            $location.path('/edit-test/' + $scope.testId);

        };

        $scope.addQuestion = function () {
            if ($scope.question.Description == null || $scope.question.Pondere == null || $scope.question.choices.length == 0) {
                alert("Toate campurile sunt obligatorii!")
                return;
            }
            $scope.questions.push($scope.question);
            $scope.question = {};
        };

        $scope.newQuestion = function () {
            $scope.question.choices = [];
        };

        $scope.addChoice = function () {
            if ($scope.choice.Description == null) {
                alert("Descrierea este obligatorie!")
                return;
            }
            var newChoice = {Description: $scope.choice.Description, Correct: $scope.choice.Correct};
            $scope.question.choices.push(newChoice);
        };


        $http.get("http://localhost:60163/tests/" + $scope.testId + "/questions", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.questions = data;

                for (var i = 0; i < $scope.questions.length; i++) {
                    $scope.getAnswers(i);
                }
            });

        $scope.getAnswers = function (i) {
            $http.get("http://localhost:60163/questions/" + $scope.questions[i].Id + "/answers", {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.questions[i].Answers = data;
                    console.log($scope.questions[i].Answers);
                });
        }
    }


});
//-------------------------------------------------------------------------------------------

userApp.controller('QuestionsController', function ($scope, $http, UserService, $location, $routeParams) {
    $scope.test = {};
    $scope.testId = $routeParams.test_id;
    $scope.questionNr = $routeParams.question_nr;
    $scope.question = {};
    $scope.questions = [];
    $scope.answer = {};
    $scope.choices = [];
    $scope.endTest = false;
    $scope.questionId = "";
    $scope.answers = [];
    $scope.choice = new Array(4);

    $scope.addQuestion = function () {
        $scope.question.test_id = $scope.testId;
        console.log($scope.question);
        $http.post("http://localhost:60163/questions", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                for (var i = 0; i < 4; i++) {
                    $scope.choice[i].question_id = data.Id;
                    $scope.comment.Date = new Date();
                    $http.post("http://localhost:60163/api/AnswerOptions", $scope.choice[i], {
                            headers: {
                                "Token": UserService.getToken()
                            }
                        })
                        .success(function () {
                        })
                        .error(function () {
                            console.log("Eroare add comment!")
                        });
                }

            })
            .error(function () {
                console.log("Eroare add discussion!")
            });
    };


    $http.get("http://localhost:60163/tests/" + $scope.testId, {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.test = data;
        }).error(function () {
        console.log("Eroare get teste!")
    });


    $http.get("http://localhost:60163/tests/" + $scope.testId + "/questions", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.questions = data;
            $scope.questionId = $scope.questions[parseInt($scope.questionNr) - 1].Id;
            $scope.question = data[parseInt($scope.questionNr) - 1];

            if ($scope.questionNr >= $scope.questions.length) {
                $scope.endTest = true;
            }

            $http.get("http://localhost:60163/questions/" + $scope.questionId + "/answers", {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function (data) {
                    $scope.choices = data;
                }).error(function () {
                console.log("Eroare get answers!")
            });

        }).error(function () {
        console.log("Eroare get intrebari!")
    });


    $scope.nextQuestion = function (data) {
        $scope.submitAnswers(data);
        $scope.questionNr = parseInt($scope.questionNr) + 1;
        $location.path('/test/' + $scope.testId + '/question/' + $scope.questionNr);
    };

    $scope.finishTest = function (data) {
        $scope.submitAnswers(data);
        $scope.getGrade();
        $scope.completed=true;
        $scope.questionNr = parseInt($scope.questionNr) + 1;
        $location.path('/tests');
    };

    $scope.getGrade = function () {
        $http.get("http://localhost:60163/grades/tests/" + $scope.testId + "/mygrade", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
            })
            .error(function () {
                console.log("Eroare put test!");
            });
    };


    $scope.submitAnswers = function (choices) {
        for (var i in choices) {
            if (choices[i].SELECTED == 'Y') {
                $scope.answer.user_Id = UserService.getUser().Id;
                $scope.answer.answerOptionId = choices[i].Id;

                $http.post("http://localhost:60163/userAnswers", $scope.answer, {
                        headers: {
                            "Token": UserService.getToken()
                        }
                    })
                    .success(function (data) {
                    })
                    .error(function () {
                        console.log("Eroare post answers!")
                    });
            }
        }
        console.log($scope.answers);
    };

});
