userApp.controller('ProfileController', function (UserService, $scope, $http, $routeParams) {
    $scope.user = {};
    $scope.oldPassword = "";
    $scope.newPassword = "";
    $scope.renewPassword = "";
    $scope.userId = $routeParams.user_id || "";

    if ($routeParams.user_id) {
        $http.get("http://localhost:60163/users/" + $scope.userId, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.user = data;
            })
            .error(function () {
                console.log("Eroare get conv!")
            });
    }
    else {
        $http.get("http://localhost:60163/users/user", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.user = data;
                if (data.group_id == null) {
                    $scope.user.group_name = "-";
                }
            })
            .error(function () {
                console.log("Eroare get conv!")
            });
    }


    $scope.resetPassword = function () {
        if ($scope.newPassword === $scope.renewPassword) {
            $http.get("http://localhost:60163/users/verifyPass/" + $scope.oldPassword,
                {
                    headers: {
                        "Token": UserService.getToken()
                    }
                })
                .success(function () {
                    $http.post("http://localhost:60163/users/resetPass/" + $scope.newPassword, {}, {
                            headers: {
                                "Token": UserService.getToken()
                            }
                        })
                        .success(function () {
                            alert("Parola a fost schimbata!")
                        })
                        .error(function () {
                            console.log("Parola nu s-a putut modifica!")
                        });
                })
                .error(function () {
                    console.log("Parola veche nu este corecta!")
                });

        } else {
            alert("Cele doua parole nu sunt la fel!");
        }


    };

});