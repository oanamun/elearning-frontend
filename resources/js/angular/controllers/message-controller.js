userApp.controller('MessagesController', function ($scope, $http, $routeParams, $location, UserService) {
    $scope.conversations = [];
    $scope.old_conversations = [];
    $scope.conversation = {};
    $scope.messages = [];
    $scope.message = {};
    $scope.convId = $routeParams.conversation_id || "";
    $scope.convIndex = 0;
    $scope.users = [];

    $http.get("http://localhost:60163/users", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.users = data;
        });

    $http.get("http://localhost:60163/userConversations", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.conversations = data;
            $scope.old_conversations = data;
            if (!$scope.convId) {
                $location.path('/messages/' + $scope.conversations[0].Id);
            }

            for (var i = 0; i < data.length; i++) {
                if ($scope.conversations[i].Id == $scope.convId) {
                    $scope.convIndex = i;
                    break;
                }
            }
        })
        .error(function () {
            console.log("Eroare get conv!")
        });


    $http.get("http://localhost:60163/conversations/" + $scope.convId + "/messages", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.messages = data;
        })
        .error(function () {
            console.log("Eroare get conv!")
        });

    $scope.isMe = function (username) {
        if (username == UserService.getUser().Username)
            return true;
        return false;
    };

    $scope.sendMessage = function () {
        if (!$scope.message.Text) {
            return;
        }
        $scope.message.Date = new Date();
        $scope.message.userConversation_id = $scope.convId;
        $http.post("http://localhost:60163/messages", $scope.message, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.messages.push(data);
                $scope.message = {};
            })
            .error(function (data, status) {
                alert("Mesajul nu a fost trimis");
            });
    };

    $scope.addConversation = function () {
        var conversation = {Name: $scope.conversation.Name};
        $http.post("http://localhost:60163/conversations", conversation, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                var userconv1 = {user_id: UserService.getUser().Id, conversation_id: data.Id}
                $http.post("http://localhost:60163/userConversations", userconv1, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                });
                var user_id = $scope.users[$scope.conversation.user].Id;
                var username = $scope.users[$scope.conversation.user].Username;
                var userconv2 = {user_id: parseInt(user_id), conversation_id: data.Id}
                $http.post("http://localhost:60163/userConversations", userconv2, {
                    headers: {
                        "Token": UserService.getToken()
                    }
                });
                $scope.conversations.push({
                    Name: data.Name,
                    Id: data.Id,
                    Users: [{Name: UserService.getUser().Username}, {Name: username}]
                });
            });
    };


    $scope.deleteConversation = function () {

        $http.delete("http://localhost:60163/conversations/" + $scope.convId, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
                $location.path('/messages');
            });
    };


    $scope.changeConversationName = function () {
        var conversation = {Id: $scope.convId, Name: $scope.conversation.Name};
        $http.put("http://localhost:60163/conversations/" + $scope.convId, conversation, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.conversations[$scope.convIndex].Name = $scope.conversation.Name;
            });
    };


    $scope.addUserToConversation = function () {
        var user_id = $scope.users[$scope.conversation.user].Id;
        var username = $scope.users[$scope.conversation.user].Username;
        var userconv = {user_id: parseInt(user_id), conversation_id: $scope.convId};
        $http.post("http://localhost:60163/userConversations", userconv, {
            headers: {
                "Token": UserService.getToken()
            }
        }).success(function (data) {
            $scope.conversations[$scope.convIndex].Users.push({Name: username});
        });
    };

    $scope.findConversation = function () {
        var new_conversations = [];
        var name = $scope.conversation.Name_search;

        if (name == null || name == "") {
            $scope.conversations = $scope.old_conversations.slice();
            console.log($scope.conversations);
        }
        else {
            for (var i = 0; i < $scope.old_conversations.length; i++) {
                if ($scope.old_conversations[i].Name.contains(name)) {
                    new_conversations.push($scope.old_conversations[i]);
                }
            }
            $scope.conversations = new_conversations;
        }
    };

    $scope.isCurrent = function (index) {
        return index == $scope.convIndex;
    };

    $scope.selectedConversation = function (index) {
        var convId = $scope.conversations[index].Id;
        $location.path('/messages/' + convId);
    }
});
