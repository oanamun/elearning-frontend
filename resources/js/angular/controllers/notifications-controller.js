userApp.controller('NotificationsController', function (UserService, $scope, $http, $routeParams) {
    $scope.notifications = {};

    $http.get("http://localhost:60163/userNotifications/mynotif", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.notifications = data;
        })
        .error(function () {
        });

    $scope.markAsSeen=function(index){
        var notId= $scope.notifications[index].Id;

        $http.get("http://localhost:60163/userNotifications/"+ notId +"/seen", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.notifications.splice(index,1);
            })
            .error(function () {
            });

    }

});