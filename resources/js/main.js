$(document).ready(function () {
    $("#button").click(function () {
        $("body").toggleClass("min-navbar");
    });

    $('.text-editor').trumbowyg({
        fullscreenable: true,
        btns: ['viewHTML',
            '|', 'formatting',
            '|', 'btnGrp-design',
            '|', 'link',
            '|', 'insertImage',
            '|', 'btnGrp-justify',
            '|', 'btnGrp-lists',
            '|', 'horizontalRule']
    });


    //// Get HTML content
    //$('#editor').trumbowyg('html');
    //// Set HTML content
    //$('#editor').trumbowyg('html', "<p>Your content here</p>");
    ////You can empty the content of the editor.
    //$('#editor').trumbowyg('empty');

    //var $tbody = $('table tbody');
    //$tbody.find('tr').sort(function(a,b){
    //    var tda = $(a).find('td:eq(1)').text(); // can replace 1 with the column you want to sort on
    //    var tdb = $(b).find('td:eq(1)').text(); // this will sort on the second column
    //    // if a < b return 1
    //    return tda < tdb ? 1
    //        // else if a > b return -1
    //        : tda > tdb ? -1
    //        // else they are equal - return 0
    //        : 0;
    //}).appendTo($tbody);
});

